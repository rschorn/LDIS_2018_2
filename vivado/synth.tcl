# #############################################################################
#
# Synthesis script for the TRNG using the Digilent Nexys 4 DDR board.
#
# #############################################################################


create_project -part xc7a100t -force design


# #############################################################################
# read necessary vhd files


# uart rx and tx library
read_vhdl ../top/uart_rx.vhd
read_vhdl ../top/uart_rx_pkg.vhd
read_vhdl ../top/uart_tx.vhd
read_vhdl ../top/uart_tx_pkg.vhd


# rng component
read_vhdl ../rng/garo.vhd
read_vhdl ../rng/lfsr.vhd
read_vhdl ../rng/rbg.vhd
read_vhdl ../rng/rng.vhd

# dummy entities for argon2
read_vhdl ../argon/team5.vhd
read_vhdl ../argon/team6.vhd

# memory controller
add_file ../argon/07/Ram2Ddr_RefComp/Source/Ram2DdrXadc_RefComp/ipcore_dir/ddr.xco
read_vhdl ../argon/07/Ram2Ddr_RefComp/Source/Ram2DdrXadc_RefComp/ram2ddrxadc_pkg.vhd
read_vhdl ../argon/07/Ram2Ddr_RefComp/Source/Ram2DdrXadc_RefComp/ram2ddrxadc.vhd
read_vhdl ../argon/07/fifo_pkg.vhd
read_vhdl ../argon/07/fifo.vhd

read_vhdl ../argon/07/Memory.vhd
read_vhdl ../argon/07/Memory_pkg.vhd



# argon2 entity
read_vhdl ../argon/Argon2.vhd

# top entity (application controller)
read_vhdl ../top/design.vhd


# #############################################################################
# read vivado constraint file


read_xdc ../design.xdc


# #############################################################################
# synthesise design with top entity argon2_application


synth_design -top argon2_application


# #############################################################################


opt_design


# #############################################################################


place_design


# #############################################################################


route_design


# #############################################################################


write_bitstream -force design.bit

