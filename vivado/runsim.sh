#!/bin/bash

# Script to simulate the Argon2 application controller VHDL design
#
# Usage:
#
#		runsim.sh (no parameters)

# Analyse uart rx and tx library
ghdl -s uart_rx.vhd
ghdl -a uart_rx.vhd

ghdl -s uart_rx_pkg.vhd
ghdl -a uart_rx_pkg.vhd

ghdl -s uart_tx.vhd
ghdl -a uart_tx.vhd

ghdl -s uart_tx_pkg.vhd
ghdl -a uart_tx_pkg.vhd


# Analyse design of top entity
ghdl -s design.vhd
ghdl -a design.vhd
ghdl -e argon2_application
ghdl -r argon2_application


# Simulate testbench
ghdl -s tb.vhd
ghdl -a tb.vhd
ghdl -e argon2_application_tb
ghdl -r argon2_application_tb --vcd=argon2_application_tb.vcd --stop-time=14ms


# Open simulation with waveform viewer
gtkwave argon2_application_tb.vcd
