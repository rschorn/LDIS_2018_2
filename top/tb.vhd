--#############################################################################
--
-- AUTHORS: Jure SOKLIC, Rupert SCHORN
--
-- This is a testbench to verify the behaviour of the Key Derivation Function
-- (KDF) Application Controller. This testbench does not cover the testing
-- of the Argon2 algorithm itself, it just verifies the UART transmit and 
-- receive functionality and the seven segment display.
--
-- A detailed verification plan is described in the protocol of task 2.
--
-- Best readability: set ts=4
--#############################################################################

library ieee;
use ieee.std_logic_1164.all;

entity argon2_application_tb is
end argon2_application_tb;

architecture behavior of argon2_application_tb is

	signal 		clk			:	std_logic := '0';
	signal		rst			:	std_logic := '0';
	signal		uart_rx		:	std_logic := '0';
	signal 		uart_tx		:	std_logic;
	signal		segment		:	std_logic_vector(7 downto 0);
	signal		an			:	std_logic_vector(7 downto 0);
	-- DDR2 interface
    signal 		ddr2_addr   : std_logic_vector(12 downto 0);
    signal 		ddr2_ba     : std_logic_vector(2 downto 0);
	signal 		ddr2_ras_n  : std_logic;
   	signal 		ddr2_cas_n  : std_logic;
    signal 		ddr2_we_n   : std_logic;
	signal 		ddr2_ck_p   : std_logic_vector(0 downto 0);
   	signal 		ddr2_ck_n   : std_logic_vector(0 downto 0);
    signal 		ddr2_cke    : std_logic_vector(0 downto 0);
	signal 		ddr2_cs_n   : std_logic_vector(0 downto 0);
  	signal 		ddr2_dm     : std_logic_vector(1 downto 0);
    signal 		ddr2_odt    : std_logic_vector(0 downto 0);
	signal 		ddr2_dq     : std_logic_vector(15 downto 0);
   	signal 		ddr2_dqs_p  : std_logic_vector(1 downto 0);
    signal 		ddr2_dqs_n  : std_logic_vector(1 downto 0);

	constant 	CLK_PERIOD	:	time := 1 us;
	constant 	BIT_PERIOD	: 	time := 26 us;
	constant	FINISH_WAIT	: 	time := 500 us;



	--#########################################################################
	--
	--PROCEDURE: 	check_uart_transmission
	--
	--PARAMETER:	-uart	simulated uart tx pin
	--				-value	value to be checked on the uart tx pin
	--
	--DESCRIPTION:	checks if the given value is transmitted over the uart tx
	--				pin
	--
	--#########################################################################
	procedure check_uart_transmission(	signal uart : std_logic; 
							constant value : std_logic_vector(7 downto 0)) is
	begin
		--check for UART start bit (= 0)
		assert uart = '0'
			report "TEST FAILED: uart_tx does not generate start bit"
			severity failure;

		--check each single bit
		for i in 0 to value'high loop
			wait for BIT_PERIOD;

			assert uart = value(i)
				report "TEST FAILED: uart_tx contains wrong data"
				severity failure;	
		end loop;

		wait for BIT_PERIOD;

		--check for UART stop bit (=1)
		assert uart = '1'
			report "TEST FAILED: uart_tx does not generate stop bit"
			severity failure;

		wait for BIT_PERIOD;

	end check_uart_transmission;


begin

	ms: entity work.argon2_application 
		port map (
			CLK => clk,
			RST => rst,
			UART_RX => uart_rx,
			UART_TX => uart_tx,
			SEGMENT => segment,
			AN => an,
			-- DDR2 interface
	        ddr2_addr => ddr2_addr,
    	    ddr2_ba => ddr2_ba,
        	ddr2_ras_n => ddr2_ras_n,
	        ddr2_cas_n => ddr2_cas_n,
    	    ddr2_we_n => ddr2_we_n,
        	ddr2_ck_p => ddr2_ck_p,
	        ddr2_ck_n => ddr2_ck_n,
    	    ddr2_cke => ddr2_cke,
        	ddr2_cs_n => ddr2_cs_n,
	        ddr2_dm => ddr2_dm,
    	    ddr2_odt => ddr2_odt,
        	ddr2_dq => ddr2_dq,
	        ddr2_dqs_p => ddr2_dqs_p,
    	    ddr2_dqs_n => ddr2_dqs_n
			);

--#############################################################################

	--create clock, duty cycle 1:1
	clk_process: process
	begin
			clk<='0';
			wait for CLK_PERIOD/2;
			clk<='1';
			wait for CLK_PERIOD/2;
	end process;

--#############################################################################


	--main test procedure
	test: process
		variable idle_bit	: std_logic := '1';
		variable start_bit	: std_logic := '0';
		variable stop_bit	: std_logic := '1';

		--string separator = x"2C" (reversed), ascii ','
		variable separator_rev	: std_logic_vector(7 downto 0) := "00110100";

		--password character = x"41" (reversed), ascii 'A'
		variable pw_rev		: std_logic_vector(7 downto 0) := "10000010";
		--iteration count character = x"35" (reversed), ascii '5'
		variable it_rev		: std_logic_vector(7 downto 0) := "10101100";
		--tag length character = x"38" (reversed), ascii '8'
		variable tag_rev	: std_logic_vector(7 downto 0) := "00011100";
		--argon type character = x"31" (reversed), ascii '1'
		variable arg_rev	: std_logic_vector(7 downto 0) := "10001100";
		
		variable pw_bits       : std_logic_vector(0 to 12) :=
			idle_bit & idle_bit & start_bit & pw_rev & stop_bit & idle_bit;
		variable it_bits       : std_logic_vector(0 to 12) :=
			idle_bit & idle_bit & start_bit & it_rev & stop_bit & idle_bit;
		variable tag_bits       : std_logic_vector(0 to 12) :=
			idle_bit & idle_bit & start_bit & tag_rev & stop_bit & idle_bit;
		variable separator_bits	: std_logic_vector(0 to 12) :=
			idle_bit & idle_bit & start_bit & separator_rev & stop_bit
		   	& idle_bit;
		variable arg_bits		: std_logic_vector(0 to 11) :=
			idle_bit & idle_bit & start_bit & arg_rev & stop_bit;


	begin
		wait for (2 * CLK_PERIOD);


		--#####################################################################
		--reset and idle state test


		--perform asynchronous reset
		rst <= '1';
		wait for (2.1 * CLK_PERIOD);
		rst <= '0';

		wait until rising_edge(clk);
		wait for 10 ns;

		--check for UART idle level
		assert uart_tx = '1'
			report "TEST FAILED: uart_tx does not indicate idle state"
			severity failure;

		--check if segment displays a '0'
		assert segment = x"03"
			report "TEST FAILED: 7 segment does not display 0"
			severity failure;

		wait until rising_edge(clk);
		wait for 500 ns;


		--#####################################################################
		--1. transmission: transmit password, iteration count, key length in 
		--bits, argon type
		--simulated ascii transmission: "AA,55,8,1"

		-- feed password bits "AA"
		for j in 0 to 1 loop
			for i in 0 to 12 loop
				uart_rx <= pw_bits(i);
				wait for BIT_PERIOD;
			end loop;
		end loop;

		--separator
		for i in 0 to 12 loop
			uart_rx <= separator_bits(i);
			wait for BIT_PERIOD;
		end loop;

		--feed iterator bits "55"
		for j in 0 to 1 loop
			for i in 0 to 12 loop
				uart_rx <= it_bits(i);
				wait for BIT_PERIOD;
			end loop;
		end loop;

		--separator
		for i in 0 to 12 loop
			uart_rx <= separator_bits(i);
			wait for BIT_PERIOD;
		end loop;

		--feed tag bits "8"
		for i in 0 to 12 loop
			uart_rx <= tag_bits(i);
			wait for BIT_PERIOD;
		end loop;

		--separator
		for i in 0 to 12 loop
			uart_rx <= separator_bits(i);
			wait for BIT_PERIOD;
		end loop;

		--feed argon type bits "1" (without idle bit after stop bit)
		for i in 0 to 11 loop
			uart_rx <= arg_bits(i);
			wait for BIT_PERIOD;
		end loop;


		--#####################################################################
		--receive key, salt and initialization vector
		--expected ascii transmission: "00,FF,00"



		--wait half bit period to perform check in the middle of the bit
		wait for BIT_PERIOD / 2;

		--check for key and separator "00,"
		check_uart_transmission(uart_tx, x"30");
		wait for CLK_PERIOD * 2;
		check_uart_transmission(uart_tx, x"30");
		wait for CLK_PERIOD * 2;
		check_uart_transmission(uart_tx, x"2C");
		wait for CLK_PERIOD * 2;

		--check for salt "FF"
		check_uart_transmission(uart_tx, x"46");
		wait for CLK_PERIOD * 2;
		check_uart_transmission(uart_tx, x"46");
		wait for CLK_PERIOD * 2;


		wait for FINISH_WAIT;



		--#####################################################################
		--2. transmission (without reset previous reset): transmit password, 
		--iteration count, key length in bits, argon type
		--simulated ascii transmission: "AA,55,8,1"

		-- feed password bits "AA"
		for j in 0 to 1 loop
			for i in 0 to 12 loop
				uart_rx <= pw_bits(i);
				wait for BIT_PERIOD;
			end loop;
		end loop;

		--separator
		for i in 0 to 12 loop
			uart_rx <= separator_bits(i);
			wait for BIT_PERIOD;
		end loop;

		--feed iterator bits "55"
		for j in 0 to 1 loop
			for i in 0 to 12 loop
				uart_rx <= it_bits(i);
				wait for BIT_PERIOD;
			end loop;
		end loop;

		--separator
		for i in 0 to 12 loop
			uart_rx <= separator_bits(i);
			wait for BIT_PERIOD;
		end loop;

		--feed tag bits "8"
		for i in 0 to 12 loop
			uart_rx <= tag_bits(i);
			wait for BIT_PERIOD;
		end loop;

		--separator
		for i in 0 to 12 loop
			uart_rx <= separator_bits(i);
			wait for BIT_PERIOD;
		end loop;

		--feed argon type bits "1" (without idle bit after stop bit)
		for i in 0 to 11 loop
			uart_rx <= arg_bits(i);
			wait for BIT_PERIOD;
		end loop;


		--#####################################################################
		--receive key, salt and initialization vector
		--expected ascii transmission: "00,FF,00"



		--wait half bit period to perform check in the middle of the bit
		wait for BIT_PERIOD / 2;

		--check for key and separator "00,"
		check_uart_transmission(uart_tx, x"30");
		wait for CLK_PERIOD * 2;
		check_uart_transmission(uart_tx, x"30");
		wait for CLK_PERIOD * 2;
		check_uart_transmission(uart_tx, x"2C");
		wait for CLK_PERIOD * 2;

		--check for salt "FF"
		check_uart_transmission(uart_tx, x"46");
		wait for CLK_PERIOD * 2;
		check_uart_transmission(uart_tx, x"46");
		wait for CLK_PERIOD * 2;


		wait for FINISH_WAIT;


		--#####################################################################

		report "TEST PASSED" severity NOTE;
		report "user forced exit of simulation" severity failure;

				
		end process;

end;
