--#############################################################################
--
-- AUTHORS: Jure SOKLIC, Rupert SCHORN
--
-- This is the entity declaration and behavior of an Application Controller
-- for a key derivation function based on an Argon2 algorithm. It instantiates
-- the Argon2 component, a needed random number generator (RNG) and UART
-- transmit and receive components. The Application Controller handles the 
-- UART communication with the host and controls the seven segment display.
--
-- Best readability: set ts=4
--#############################################################################



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;



--#############################################################################



entity argon2_application is

	port (
		CLK		: 	in std_logic;	-- clock
		RST		: 	in std_logic;	-- asynchronous reset
		UART_RX	: 	in std_logic;	-- UART receive pin
		UART_TX	: 	out std_logic;	-- UART transmission pin
		SEGMENT : 	out std_logic_vector(7 downto 0); -- 7seg display
		AN		: 	out std_logic_vector(7 downto 0);  -- 7seg display anodes
		-- DDR2 interface
        ddr2_addr            : out   std_logic_vector(12 downto 0);
        ddr2_ba              : out   std_logic_vector(2 downto 0);
        ddr2_ras_n           : out   std_logic;
        ddr2_cas_n           : out   std_logic;
        ddr2_we_n            : out   std_logic;
        ddr2_ck_p            : out   std_logic_vector(0 downto 0);
        ddr2_ck_n            : out   std_logic_vector(0 downto 0);
        ddr2_cke             : out   std_logic_vector(0 downto 0);
        ddr2_cs_n            : out   std_logic_vector(0 downto 0);
        ddr2_dm              : out   std_logic_vector(1 downto 0);
        ddr2_odt             : out   std_logic_vector(0 downto 0);
        ddr2_dq              : inout std_logic_vector(15 downto 0);
        ddr2_dqs_p           : inout std_logic_vector(1 downto 0);
        ddr2_dqs_n           : inout std_logic_vector(1 downto 0)
	);

end argon2_application;



--#############################################################################



architecture behavioral of argon2_application is

	--for testbench: CLK_FREQ must be set to 1E6
	constant CLK_FREQ    	: integer := 100E6;	-- clock frequency
	constant BAUDRATE    	: integer := 38400; -- UART baudrate

	constant MAX_PASSWORD_LEN_BYTE	: integer := 256;
	constant MAX_ITERATION_COUNT   	: integer := 1E6;
	constant MAX_TAG_LEN_BYTE      	: integer := 1024;

	--max ascii string length for displaying MAX_ITERATION_COUNT
	constant MAX_ITERATION_COUNT_STRING_LENGTH	: integer := 7;
	--max ascii string length for displaying MAX_TAG_LEN_BYTE
	constant MAX_TAG_LEN_STRING_LENGTH			: integer := 4;

	--for testbench: SALT_LEN_BYTE must be set to 1
    constant SALT_LEN_BYTE     		: integer := 16;
	
	--ascii string separator ','
	constant STRING_SEPARATOR		: std_logic_vector(7 downto 0) := x"2C";

	--(dummy) seed input for RNG
	constant RNG_SEED	: std_logic_vector(SALT_LEN_BYTE * 8 - 1 downto 0) 
							:= (others => '1');
	-- polynom for random bit generator
    constant RO_POLYNOM : std_logic_vector := "100011101";
	

--#############################################################################


    component clk_wiz_0
    port
     (-- Clock in ports
      -- Clock out ports
      clk_out1          : out    std_logic;
      -- Status and control signals
      reset             : in     std_logic;
      clk_in1           : in     std_logic
     );
    end component;

	--component declaration for the random number generator
	--used RNG: Group 12: Valentin Hanser, Andreas Hirtenlehner
	--for testbench: this component declaration must be commented
	component rng is
		generic (
			length     : integer;
			ro_polynom : std_logic_vector
		);
		port (
			clk           : in  std_logic;
			reset         : in  std_logic;
            seed          : in  std_logic_vector(length-1 downto 0);
            random_number : out std_logic_vector(length-1 downto 0)
        );
    end component;


	--component declaration for the KDF algorithm (Argon2)
	--component is provided by Group 12: Valentin Hanser, Andreas Hirtenlehner
	--for testbench: this component declaration must be excluded (commented)
	component Argon2 is
		generic (
			DEG_OF_PARALLELISM    : integer;
        	MAX_TAG_LEN_BYTE      : integer;
	        MAX_PASSWORD_LEN_BYTE : integer;
	        MAX_SALT_LEN_BYTE     : integer;
        	MAX_ITERATION_COUNT   : integer
		);
		port (
			clk						: in std_logic;
			clk_200mhz				: in std_logic;
			reset					: in std_logic;
			start					: in std_logic;
			password          		: 
				in std_logic_vector(MAX_PASSWORD_LEN_BYTE * 8 - 1 downto 0);
        	password_len_byte	 	: 
				in integer range 0 to MAX_PASSWORD_LEN_BYTE;
       	iteration_count 		: 
				in integer range 0 to MAX_ITERATION_COUNT;
        	tag_len_byte 			: in integer range 0 to MAX_TAG_LEN_BYTE;
        	argon_type 				: in integer range 0 to 2;
        	salt         			: 
				in std_logic_vector(SALT_LEN_BYTE * 8 - 1 downto 0);
        	salt_len_byte	 		: in integer range 0 to SALT_LEN_BYTE;
        	tag 					: 
				out std_logic_vector(MAX_TAG_LEN_BYTE * 8 - 1 downto 0);
        	ready 					: out std_logic;
        	current_iteration_count	: 
				out integer range 0 to MAX_ITERATION_COUNT;
			-- DDR2 interface
        	ddr2_addr            : out   std_logic_vector(12 downto 0);
        	ddr2_ba              : out   std_logic_vector(2 downto 0);
	        ddr2_ras_n           : out   std_logic;
    	    ddr2_cas_n           : out   std_logic;
        	ddr2_we_n            : out   std_logic;
	        ddr2_ck_p            : out   std_logic_vector(0 downto 0);
    	    ddr2_ck_n            : out   std_logic_vector(0 downto 0);
        	ddr2_cke             : out   std_logic_vector(0 downto 0);
	        ddr2_cs_n            : out   std_logic_vector(0 downto 0);
    	    ddr2_dm              : out   std_logic_vector(1 downto 0);
        	ddr2_odt             : out   std_logic_vector(0 downto 0);
	        ddr2_dq              : inout std_logic_vector(15 downto 0);
    	    ddr2_dqs_p           : inout std_logic_vector(1 downto 0);
        	ddr2_dqs_n           : inout std_logic_vector(1 downto 0)
		);
	end component;


--#############################################################################


    --##### CLOCK #####
    signal clock_200MHz         : std_logic;

	--##### UART #####

	--receive data and control signals
	signal received_data		: std_logic_vector(7 downto 0);
	signal received_data_ready	: std_logic;
	--transmit data and control signals
	signal tx_send				: std_logic;
	signal tx_send_next			: std_logic;
	signal tx_data				: std_logic_vector(7 downto 0);
	signal tx_data_next			: std_logic_vector(7 downto 0);
	signal uart_tx_ready		: std_logic;

	-- UART Receive State Machine
    type rx_state_t is (
        STATE_IDLE,
        STATE_READ_DATA,
        STATE_WAIT
    );
    signal rx_state      : rx_state_t := STATE_IDLE;
    signal rx_state_next : rx_state_t := STATE_IDLE;

	-- UART Transmit State Machine
    type tx_state_t is (
        STATE_IDLE,
        STATE_WRITE_DATA,
		STATE_PRESERVE_TX_SEND,
        STATE_WAIT
    );
    signal tx_state      : tx_state_t := STATE_IDLE;
    signal tx_state_next : tx_state_t := STATE_IDLE;

	-- UART Receive Data Semantic
	type rx_data_semantic_t is (
		STATE_PASSWORD,
		STATE_ITERATION_COUNT,
		STATE_TAG_LENGTH,
		STATE_ALGO_TYPE
	);
	signal rx_data_semantic			: rx_data_semantic_t := STATE_PASSWORD;
	signal rx_data_semantic_next	: rx_data_semantic_t := STATE_PASSWORD;

	-- UART Transmit Data Semantic
	type tx_data_semantic_t is (
		STATE_KEY,
		STATE_SALT
	);
	signal tx_data_semantic			: tx_data_semantic_t := STATE_KEY;
	signal tx_data_semantic_next	: tx_data_semantic_t := STATE_KEY;

	--data semantics: password
	signal password					: 
		std_logic_vector(MAX_PASSWORD_LEN_BYTE * 8 - 1 downto 0);
	signal password_next			: 
		std_logic_vector(MAX_PASSWORD_LEN_BYTE * 8 - 1 downto 0);
    signal password_len_byte 		: integer range 0 to MAX_PASSWORD_LEN_BYTE;
	signal password_len_byte_next 	: integer range 0 to MAX_PASSWORD_LEN_BYTE;
	signal password_len_buffer		: integer range 0 to MAX_PASSWORD_LEN_BYTE;
	signal password_len_buffer_next	: integer range 0 to MAX_PASSWORD_LEN_BYTE;

	--data semantics: iteration count
	signal iteration_count_vector		: 
		std_logic_vector(MAX_ITERATION_COUNT_STRING_LENGTH * 4 - 1 downto 0);
	signal iteration_count_vector_next	: 
		std_logic_vector(MAX_ITERATION_COUNT_STRING_LENGTH * 4 - 1 downto 0);
	signal iteration_count 						: 
		integer range 0 to MAX_ITERATION_COUNT;
	signal iteration_conv_counter 				: 
		integer range 0 to MAX_ITERATION_COUNT_STRING_LENGTH;
	signal iteration_count_string_length		: 
		integer range 0 to MAX_ITERATION_COUNT_STRING_LENGTH;
	signal iteration_count_string_length_next	: 
		integer range 0 to MAX_ITERATION_COUNT_STRING_LENGTH;
	signal enable_it_conv						: std_logic;
	signal enable_it_conv_next					: std_logic;

	--data semantics: key length (tag length)
	signal tag_len_vector				:
		std_logic_vector(MAX_TAG_LEN_STRING_LENGTH * 4 - 1 downto 0);
	signal tag_len_vector_next			:
		std_logic_vector(MAX_TAG_LEN_STRING_LENGTH * 4 - 1 downto 0);
	signal tag_len_bits 				: 
		integer range 0 to MAX_TAG_LEN_BYTE * 8;
	signal tag_len_conv_counter	 		: 
		integer range 0 to MAX_TAG_LEN_STRING_LENGTH;
	signal tag_len_string_length		: 
		integer range 0 to MAX_TAG_LEN_STRING_LENGTH;
	signal tag_len_string_length_next	: 
		integer range 0 to MAX_TAG_LEN_STRING_LENGTH;
	signal enable_tag_conv				: std_logic;
	signal enable_tag_conv_next			: std_logic;	

	--data semantics: argon type
	signal argon_type 		: integer range 0 to 2;
	signal argon_type_next	: integer range 0 to 2;



	--nibble counter for key transmission
	signal current_tag_nibble			: 
		integer range 0 to MAX_TAG_LEN_BYTE * 2;
	signal current_tag_nibble_next		: 
		integer range 0 to MAX_TAG_LEN_BYTE * 2;
	--nibble counter for salt transmission
	signal current_salt_nibble			: 
		integer range 0 to SALT_LEN_BYTE * 2;
	signal current_salt_nibble_next		: 
		integer range 0 to SALT_LEN_BYTE * 2;



	--##### ARGON2 #####

	signal random_number            : 
		std_logic_vector(SALT_LEN_BYTE * 8 - 1 downto 0) := (others => '1');
	signal random_number_buffer		:
		std_logic_vector(SALT_LEN_BYTE * 8 - 1 downto 0);
	signal random_number_buffer_next	:
		std_logic_vector(SALT_LEN_BYTE * 8 - 1 downto 0);
    signal tag 						: 
		std_logic_vector(MAX_TAG_LEN_BYTE * 8 - 1 downto 0) := (others => '0');
    signal ready 					: std_logic := '0';
    signal current_iteration_count	: integer range 0 to MAX_ITERATION_COUNT;
	signal start					: std_logic;
	signal start_next				: std_logic;

	--synchronization signals for detecting rising edge of ready signal
	signal ready_sync_1			: std_logic;
	signal ready_sync_2			: std_logic;
	


	--##### 7 SEGMENT #####

	-- for dividing frequency to 500 Hz
	signal prescaler					: unsigned(19 downto 0); 
	signal segment_clock_enable			: std_logic;

	--anode selection of 7 segments
	signal an_selector					: std_logic_vector(7 downto 0);

	--hex coded digit of one 7 segment number
	signal digit						: std_logic_vector(3 downto 0);

	signal calc_tag_len_byte           : integer range 1 to MAX_TAG_LEN_BYTE;
begin

    clock_divider : clk_wiz_0
    port map
     (-- Clock in ports
      -- Clock out ports
      clk_out1 => clock_200MHz,
      -- Status and control signals
      reset => RST,
      clk_in1 => CLK
     );

	--instantiate UART receive component
	uart_receive : entity work.uart_rx
		generic map(
			CLK_FREQ => CLK_FREQ,
			BAUDRATE => BAUDRATE
		)

		port map(
			clk   		=> CLK,
			rst   		=> RST,
			rx  		=> UART_RX,
			data  		=> received_data,
			data_new	=> received_data_ready
		);

	--instantiate UART transmit component
	uart_transmit: entity work.uart_tx
        generic map(
			CLK_FREQ => CLK_FREQ,
			BAUDRATE => BAUDRATE
		)

		port map(
			clk   => CLK,
			rst   => RST,
			send  => tx_send,
			data  => tx_data,
			rdy   => uart_tx_ready,
			tx    => UART_TX
		); 


	--instantiate random number generator
	--for testbench: component instantiation must be commented
	rng_inst: entity work.rng
        generic map(
			length => SALT_LEN_BYTE * 8, 
			ro_polynom => RO_POLYNOM
		)

        port map(
			clk 			=> CLK, 
			reset 			=> RST, 
			seed		 	=> RNG_SEED, 
			random_number 	=> random_number
		);
	
	
	
	
	--instantiate key derivation function component
	--for testbench: component instantiation must be commented
	argon2_inst: entity work.Argon2
		generic map(
			DEG_OF_PARALLELISM    => 4,
        	MAX_TAG_LEN_BYTE      => MAX_TAG_LEN_BYTE,
	        MAX_PASSWORD_LEN_BYTE => MAX_PASSWORD_LEN_BYTE,
	        MAX_SALT_LEN_BYTE     => SALT_LEN_BYTE,
        	MAX_ITERATION_COUNT   => MAX_ITERATION_COUNT

		)

		port map(
			clk => CLK,
			clk_200mhz => clock_200MHz,
			reset => RST,
			start => start,
			password => password,
			password_len_byte => password_len_buffer,
			iteration_count => iteration_count,
			tag_len_byte => calc_tag_len_byte,
			argon_type => argon_type,
			salt => random_number_buffer,
			salt_len_byte => SALT_LEN_BYTE,
        	tag => tag,
        	ready => ready,
        	current_iteration_count => current_iteration_count,
			-- DDR2 interface
	        ddr2_addr => ddr2_addr,
    	    ddr2_ba => ddr2_ba,
        	ddr2_ras_n => ddr2_ras_n,
	        ddr2_cas_n => ddr2_cas_n,
    	    ddr2_we_n => ddr2_we_n,
        	ddr2_ck_p => ddr2_ck_p,
	        ddr2_ck_n => ddr2_ck_n,
    	    ddr2_cke => ddr2_cke,
        	ddr2_cs_n => ddr2_cs_n,
	        ddr2_dm => ddr2_dm,
    	    ddr2_odt => ddr2_odt,
        	ddr2_dq => ddr2_dq,
	        ddr2_dqs_p => ddr2_dqs_p,
    	    ddr2_dqs_n => ddr2_dqs_n
		);



--#############################################################################


	--data synchronization (clocked)
	process(CLK,RST)
	begin

		if(RST = '1') then

			rx_state <= STATE_IDLE;
			rx_data_semantic <= STATE_PASSWORD;
			tx_state <= STATE_IDLE;
			tx_data_semantic <= STATE_KEY;

			tx_send <= '0';
			tx_data <= (others => '0');

			password <= (others => '0');
			password_len_byte <= 0;
			password_len_buffer <= 0;

			iteration_count_vector <= (others => '0');
			iteration_count_string_length <= 0;
			enable_it_conv <= '0';

			tag_len_vector <= (others => '0');
			tag_len_string_length <= 0;
			enable_tag_conv <= '0';

			argon_type <= 0;
			
			start <= '0';
	
			current_tag_nibble <= 0;
			current_salt_nibble <= 0;

			ready_sync_1 <= '0';
			ready_sync_2 <= '0';

			random_number_buffer <= (others => '1');

		elsif(rising_edge(CLK)) then

			rx_state <= rx_state_next;
			rx_data_semantic <= rx_data_semantic_next;
			tx_state <= tx_state_next;
			tx_data_semantic <= tx_data_semantic_next;

			tx_send <= tx_send_next;
			tx_data <= tx_data_next;

			password <= password_next;
			password_len_byte <= password_len_byte_next;
			password_len_buffer <= password_len_buffer_next;

			iteration_count_vector <= iteration_count_vector_next;
			iteration_count_string_length <= iteration_count_string_length_next;
			enable_it_conv <= enable_it_conv_next;

			tag_len_vector <= tag_len_vector_next;
			tag_len_string_length <= tag_len_string_length_next;
			enable_tag_conv <= enable_tag_conv_next;

			argon_type <= argon_type_next;

			start <= start_next;
			
			current_tag_nibble <= current_tag_nibble_next;
			current_salt_nibble <= current_salt_nibble_next;

			ready_sync_1 <= ready;
			ready_sync_2 <= ready_sync_1;

			random_number_buffer <= random_number_buffer_next;

		end if;

	end process;


--#############################################################################


	--UART receive handling (combinatorial)
	rx_state_machine : process (	rx_state, 
									received_data_ready, 
									rx_data_semantic, 
									received_data, 
									password_len_byte, 
									password_len_buffer, 
									iteration_count_string_length, 
									tag_len_string_length, 
									password, 
									argon_type, 
									iteration_count_vector, 
									tag_len_vector, 
									enable_it_conv, 
									enable_tag_conv, 
									start,
									random_number,
									random_number_buffer)
    begin
		
		--prevent latches
		rx_state_next <= rx_state;
		rx_data_semantic_next <= rx_data_semantic;

		password_next <= password;
		password_len_byte_next <= password_len_byte;
		password_len_buffer_next <= password_len_buffer;

		iteration_count_string_length_next <= iteration_count_string_length;

		tag_len_string_length_next <= tag_len_string_length;

		argon_type_next <= argon_type;

		iteration_count_vector_next <= iteration_count_vector;
		tag_len_vector_next <= tag_len_vector;

		enable_it_conv_next <= enable_it_conv;
		enable_tag_conv_next <= enable_tag_conv;

		start_next <= start;
		random_number_buffer_next <= random_number_buffer;

		--for testbench: the following line must be commented out
		--ready <= '0';

        case rx_state is
            when STATE_IDLE => 
				--disable data conversion
				enable_it_conv_next <= '0';
				enable_tag_conv_next <= '0';	

				--reset counters
				password_len_byte_next <= 0;
				iteration_count_string_length_next <= 0;
				tag_len_string_length_next <= 0;

                if(received_data_ready = '1') then
                    rx_state_next <= STATE_READ_DATA;
                end if;

				--disable KDF calculation
				start_next <= '0';

            when STATE_READ_DATA =>
				rx_state_next <= STATE_WAIT;

				case rx_data_semantic is
					--receiving password
					when STATE_PASSWORD =>
						if(received_data = STRING_SEPARATOR) then
							--password received
							rx_data_semantic_next <= STATE_ITERATION_COUNT;
							--buffer password
							password_len_buffer_next <= password_len_byte;
						else
							--password transmission not finished yet
							password_next(((password_len_byte + 1) * 8 - 1) 
								downto (password_len_byte * 8)) 
								<= received_data;
							password_len_byte_next <= password_len_byte + 1;
						end if;
					--receiving iteration count
					when STATE_ITERATION_COUNT =>
						if(received_data = STRING_SEPARATOR) then
							--iteration count received
							rx_data_semantic_next <= STATE_TAG_LENGTH;
							--start ascii to integer conversion
							enable_it_conv_next <= '1';
						else
							--iteration count transmission not finished yet
							iteration_count_vector_next(
								((MAX_ITERATION_COUNT_STRING_LENGTH - 
								iteration_count_string_length) * 4 - 1) 
								downto ((MAX_ITERATION_COUNT_STRING_LENGTH - 
								iteration_count_string_length - 1) * 4)) 
								<= received_data(3 downto 0);
							iteration_count_string_length_next 
								<= iteration_count_string_length + 1;
						end if;
					--receiving tag length
					when STATE_TAG_LENGTH =>
						if(received_data = STRING_SEPARATOR) then
							--tag length received
							rx_data_semantic_next <= STATE_ALGO_TYPE;
							--start ascii to integer conversion
							enable_tag_conv_next <= '1';
						else
							--tag length transmission not finished yet
							tag_len_vector_next(
								((MAX_TAG_LEN_STRING_LENGTH - 
								tag_len_string_length) * 4 - 1) 
								downto ((MAX_TAG_LEN_STRING_LENGTH - 
								tag_len_string_length - 1) * 4)) 
								<= received_data(3 downto 0);
							tag_len_string_length_next 
								<= tag_len_string_length + 1;
						end if;
					--receiving algorithm type
					when STATE_ALGO_TYPE =>
						argon_type_next <= to_integer(
							unsigned(received_data(3 downto 0)));
						rx_state_next <= STATE_IDLE;
						rx_data_semantic_next <= STATE_PASSWORD;
						--data receive finished, start KDF calc.
						start_next <= '1';
						random_number_buffer_next <= random_number;
						--for testbench: the following line must be commented out
						--ready <= '1';
				end case;

            when STATE_WAIT =>
				--wait until new data byte available
                if(received_data_ready = '1') then
                    rx_state_next <= STATE_READ_DATA;
                end if;
		end case;
    end process rx_state_machine;


--#############################################################################


	--UART transmit handling (combinatorial)
	tx_state_machine : process(	tx_state, 
								tx_data_semantic, 
								current_tag_nibble, 
								tag_len_bits, 
								tag, 
								current_salt_nibble, 
								random_number,  
								tx_send, 
								tx_data, 
								uart_tx_ready, 
								start, 
								ready_sync_1, 
								ready_sync_2)
		variable nibble_temp : std_logic_vector(3 downto 0);
	begin
		--prevent latches
		tx_state_next <= tx_state;
		tx_send_next <= tx_send;
		tx_data_next <= tx_data;
		tx_data_semantic_next <= tx_data_semantic;
		current_tag_nibble_next <= current_tag_nibble;
		current_salt_nibble_next <= current_salt_nibble;

		case tx_state is
			when STATE_IDLE =>
				tx_send_next <= '0';
				--if UART transmission component is ready and rising edge
				--occured on the ready signal of the Argon2 component, start
				--transmission
				if(	uart_tx_ready = '1' and 
					ready_sync_1 = '1' and ready_sync_2 = '0') then --dirty solution?
					tx_state_next <= STATE_WRITE_DATA;
				end if;

				--reset counter
				current_tag_nibble_next <= 0;
				current_salt_nibble_next <= 0;

			when STATE_WRITE_DATA =>
				tx_state_next <= STATE_PRESERVE_TX_SEND;
				tx_send_next <= '1';

				case tx_data_semantic is
					--transmit key
					when STATE_KEY =>
						if(current_tag_nibble >= tag_len_bits * 2 / 8) then
							--key transmission finished, send separator
							tx_data_next <= STRING_SEPARATOR;
							tx_data_semantic_next <= STATE_SALT;
						else
							--key transmission in progress
							nibble_temp := tag(tag_len_bits - 
								current_tag_nibble * 4 - 1 downto 
								tag_len_bits - (current_tag_nibble + 1) * 4);

							--convert binary to ascii
							if(nibble_temp > "1001") then
								tx_data_next <= "0100" & std_logic_vector(
												unsigned(nibble_temp) - 9);
							else
								tx_data_next <= "0011" & nibble_temp;
							end if;

							current_tag_nibble_next <= current_tag_nibble + 1;
						end if;
					--transmit salt
					when STATE_SALT =>
						if(current_salt_nibble >= SALT_LEN_BYTE * 2) then
							--salt transmission finished, no separator
							tx_state_next <= STATE_IDLE;
							tx_data_semantic_next <= STATE_KEY;
							tx_send_next <= '0';	
						else
							--salt transmission in progress
							nibble_temp := random_number(SALT_LEN_BYTE * 8 
								- current_salt_nibble * 4 - 1 downto 
								SALT_LEN_BYTE * 8 - 
								(current_salt_nibble + 1) * 4);

							--convert binary to ascii
							if(nibble_temp > "1001") then
								tx_data_next <= "0100" & std_logic_vector(
												unsigned(nibble_temp) - 9);
							else
								tx_data_next <= "0011" & nibble_temp;
							end if;

							current_salt_nibble_next <= 
								current_salt_nibble + 1;
						end if;
				end case;
			when STATE_PRESERVE_TX_SEND =>
				--wait one more cycle before resetting tx_send
				tx_state_next <= STATE_WAIT;
			when STATE_WAIT =>
				tx_send_next <= '0';
				if(uart_tx_ready = '1') then
					tx_state_next <= STATE_WRITE_DATA;
				end if;
		end case;
	end process tx_state_machine;


--#############################################################################


	--convert received iteration count from ascii to integer (clocked)
	iteration_count_conversion : process(RST, CLK)
	begin

		if(RST = '1') then

			iteration_conv_counter <= 0;
			iteration_count <= 0;

		elsif(rising_edge(CLK)) then

			if(enable_it_conv = '1') then

				if(iteration_conv_counter <= 
					iteration_count_string_length - 1) then

					iteration_count <= 10 * iteration_count + 
						to_integer(unsigned(iteration_count_vector(
						((MAX_ITERATION_COUNT_STRING_LENGTH - 
						iteration_conv_counter) * 4 - 1) downto 
						((MAX_ITERATION_COUNT_STRING_LENGTH - 
						iteration_conv_counter - 1) * 4))));

					iteration_conv_counter <= iteration_conv_counter + 1;
				end if;

			end if;

		end if;

	end process iteration_count_conversion;


--#############################################################################


	--convert received tag length from ascii to integer (clocked)
	tag_len_conversion : process(RST, CLK)
	begin

		if(RST = '1') then

			tag_len_conv_counter <= 0;
			tag_len_bits <= 0;

		elsif(rising_edge(CLK)) then

			if(enable_tag_conv = '1') then

				if(tag_len_conv_counter <= tag_len_string_length - 1) then

					tag_len_bits <= 10 * tag_len_bits + to_integer(
						unsigned(tag_len_vector(
						((MAX_TAG_LEN_STRING_LENGTH - tag_len_conv_counter) * 4
					   	- 1) downto ((MAX_TAG_LEN_STRING_LENGTH - 
						tag_len_conv_counter - 1) * 4))));

					tag_len_conv_counter <= tag_len_conv_counter + 1;
					
					calc_tag_len_byte <= tag_len_bits / 8;

				end if;
            
			end if;
			
			if(tx_data_semantic = STATE_SALT) then
                tag_len_bits <= 0;
            end if;

		end if;

	end process tag_len_conversion;

	

--#############################################################################


	--7 segment clock enable generation for multiplexing (clocked)
	segment_clock_generation: process(RST, CLK)
	begin

		if(RST = '1') then

			--asynchronous reset --> reset signals
			prescaler <= (others => '0');
			segment_clock_enable <= '0';

		elsif(rising_edge(CLK)) then

			--x30D40 = d200000 for 100MHz
			if(prescaler = CLK_FREQ/500) then
				prescaler <= (others => '0');

				--create a single impuls every 2ms (f = 500Hz, T = 2ms)
				segment_clock_enable <= '1';
			else
				prescaler <= prescaler + 1;

				segment_clock_enable <= '0';
			end if;

		end if;

	end process segment_clock_generation;

--#############################################################################


	--bcd to 7 segment coder (combinatorial)
	seven_segment_encoder: process(digit)
	begin

		case  digit is
			--------------------------abcdefg.----------
			when "0000"=> SEGMENT <="00000011";  -- '0'
			when "0001"=> SEGMENT <="10011111";  -- '1'
			when "0010"=> SEGMENT <="00100101";  -- '2'
			when "0011"=> SEGMENT <="00001101";  -- '3'
			when "0100"=> SEGMENT <="10011001";  -- '4'
			when "0101"=> SEGMENT <="01001001";  -- '5'
			when "0110"=> SEGMENT <="01000001";  -- '6'
			when "0111"=> SEGMENT <="00011111";  -- '7'
			when "1000"=> SEGMENT <="00000001";  -- '8'
			when "1001"=> SEGMENT <="00001001";  -- '9'
			when "1010"=> SEGMENT <="00010000";  -- 'A'
			when "1011"=> SEGMENT <="00000000";  -- 'B'
			when "1100"=> SEGMENT <="01100010";  -- 'C'
			when "1101"=> SEGMENT <="00000010";  -- 'D'
			when "1110"=> SEGMENT <="01100000";  -- 'E'
			when "1111"=> SEGMENT <="01110000";  -- 'F'

			when others => SEGMENT <= "11111111";
		end case;

	end process seven_segment_encoder;


--#############################################################################


	--multiplexing common anode signals for 7 segment display (combinatorial)
	segment_multiplexer: process(	an_selector,
									iteration_count,
									current_iteration_count)
	begin
		--prevent latches
		digit <= "0000";
		
		--display the last 4 bytes of the last random number	
		case an_selector is

			when "11111110" => digit <= std_logic_vector(
				to_unsigned(current_iteration_count, 32)(3 downto 0));
			when "11111101" => digit <= std_logic_vector(
				to_unsigned(current_iteration_count, 32)(7 downto 4));
			when "11111011" => digit <= std_logic_vector(to_unsigned(
				current_iteration_count, 32)(11 downto 8));
			when "11110111" => digit <= std_logic_vector(to_unsigned(
				current_iteration_count, 32)(15 downto 12));
			when "11101111" => digit <= std_logic_vector(to_unsigned(
				current_iteration_count, 32)(19 downto 16));
			when "11011111" => digit <= std_logic_vector(to_unsigned(
				current_iteration_count, 32)(23 downto 20));
			when "10111111" => digit <= std_logic_vector(to_unsigned(
				current_iteration_count, 32)(27 downto 24));
			when "01111111" => digit <= std_logic_vector(to_unsigned(
				current_iteration_count, 32)(31 downto 28));

			when others => digit <= "0000";

		end case;
		
		AN <= an_selector;

	end process segment_multiplexer;


--#############################################################################


	--one hot coding for 7 segment digit multiplexing (clocked)
	common_anode_selector: process(RST,CLK)
	begin

		if(RST = '1') then

			an_selector <= "11111110";

		elsif(rising_edge(CLK)) then

			--if segment clock enable pulse is present, shift current selected
			--anode to the left
			if(segment_clock_enable = '1') then
				an_selector <= an_selector(6 downto 0) & an_selector(7);
			end if;

		end if;

	end process common_anode_selector;	
end behavioral;
